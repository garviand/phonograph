import './Clone.js'; // need to import Clone before Clip

export { default as Clip } from './Clip.js';
export { default as getContext } from './getContext.js';
export { default as init } from './init.js';
